#include <SPI.h>
#include <RF24.h>

// For Radio
RF24 radio(7, 8); // CE, CSN
const uint64_t pipe = 0xE8E8F0F0E1LL;
String slaveReply = String("Slave 00 ");
const int BUFFER_SIZE = 64;

// For Ultrasonic
const int trigPin = 9;
const int echoPin = 10;

// For Temperature
const int tempPin = A7;

void radioRxMode() {
  radio.begin();
  radio.openReadingPipe(0, pipe);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();
}

void radioTxMode() {
  radio.begin();
  radio.openWritingPipe(pipe);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();
}

void setup() {
  Serial.begin(9600);

  // Radio
  radioRxMode();

  // Ultrasonic
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  //Temperature
  pinMode(tempPin, INPUT);
}

void loop() {
  char rdTxt[BUFFER_SIZE];
  
  if(radio.available()) {
    radio.read(&rdTxt, sizeof(rdTxt));
    String rcvd(rdTxt);
    Serial.println(rcvd);
    
    if(rcvd.equals("Query 00")) {
      radioTxMode();
      
      char replyDist[BUFFER_SIZE];
      (slaveReply + String(getDistance())).toCharArray(replyDist, BUFFER_SIZE);
      radio.write(&replyDist, sizeof(replyDist));

      char replyTemp[BUFFER_SIZE];
      (slaveReply + String(getTemperature())).toCharArray(replyTemp, BUFFER_SIZE);
      radio.write(&replyTemp, sizeof(replyTemp));
      
      radioRxMode();
    }    
  }
}

double dist(long dur) {
  return 0.0173 * dur -0.0801;
}

double getDistance() {
  long val = 0;
  long duration;

  for (int i = 0; i < 25; i++) {
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    
    duration = pulseIn(echoPin, HIGH);
    
    //Serial.print("Time: ");
    //Serial.println(duration);
    val += duration;
    delay(25);
  }
  
  double value = dist(((double)val / 25.0));
  
  Serial.print("Distance: ");
  Serial.println(value);
  
  val = 0;

  return value;
}

double temp(int input) {
  return 8257 
          - 520 * input 
          + 12.2 * input * input 
          - 0.128 * input * input * input 
          + 0.000498 * input * input * input * input;
  //return input * 0.636804657179819 - 2.76681759379045;
}

double getTemperature() {
  double value = temp(analogRead(tempPin));

  Serial.print("Temperature: ");
  Serial.println(value);

  return value;
}
